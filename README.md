# Flappy Bird Clone

## Preview

| ![screenshot1](/Assets/Screenshots/Screenshot1.png)  | ![screenshot2](/Assets/Screenshots/Screenshot2.png) | ![screenshot3](/Assets/Screenshots/Screenshot3.png) |
|:---:|:---:|:---:|
| Screenshot 1 | Screenshot 2 | Screenshot 3 |
| ![screenshot4](/Assets/Screenshots/Screenshot4.png)  | ![screenshot5](/Assets/Screenshots/Screenshot5.png) |
| Screenshot 4 | Screenshot 5 |

## Description

This project is a clone, made by me, of the game "Flappy Bird". 
I've made it just for studies purposes, feel free to download, to test, to use or whatever you want. (:


ps.: Don't forget to switch platform to android.

## Credits

Graphics by Kenney Vleugels (www.kenney.nl)