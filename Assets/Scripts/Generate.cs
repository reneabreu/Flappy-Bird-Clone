﻿using UnityEngine;
using System.Collections;

public class Generate : MonoBehaviour {

    [Tooltip("Obstacle prefab")]
    public GameObject obstacle;                         // Obstacle prefab
    [Tooltip("Starting time to spawn in seconds")]
    public float startingTime = 1f;                     // Spawn first obstacle in X seconds
    [Tooltip("Repeat spawn in seconds")]
    public float repeatingTime = 1.25f;                 // Spawn obstacles in X seconds
	
	// Use this for initialization
	void Start(){
        // Start invoking obstacles
		InvokeRepeating("CreateObstacle", startingTime, repeatingTime);
	}

	void CreateObstacle(){
        // Instatiate new obstacle
		Instantiate(obstacle);
	}
}
