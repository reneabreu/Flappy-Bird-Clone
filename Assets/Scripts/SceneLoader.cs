﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour 
{
    // Choose Scene to load
	public void LoadScene(int SceneIndex)
	{
        // Load Scene
		SceneManager.LoadScene (SceneIndex);
	}

    // Quit Game
    public void Quit()
    {
        Application.Quit();
    }
}
