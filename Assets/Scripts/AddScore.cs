﻿using UnityEngine;
using System.Collections;

public class AddScore : MonoBehaviour {

    // Check Collision
	void OnTriggerEnter2D(Collider2D colisao)
	{
		if (colisao.gameObject.tag == "Player") {
            // Call event
            ScoreManager.Instance.IncreaseScore();
            // Destroy this GameObject
			//Destroy (this.gameObject);
		}
	}
}
