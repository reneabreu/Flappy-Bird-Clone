﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour {

    private int ScoreInt = 0;                           // Score to count
    public Text ScoreText;                              // Text to show score to the player
    public Text ScoreDead;                              // Text to show score to the player on Game Over Panel
    public Text HighScore;                              // High score text
    public GameObject GameOverPanel;                    // Game Over Panel to show
     
    // Use this for initialization
    void Start () {

        // Score int to text
        ScoreText.text = ScoreInt.ToString();
	}

    public void IncreaseScore()
    {
        // Increase Score by one
        ScoreInt++;
        // Change Score on screen after it was increased
        ScoreText.text = ScoreInt.ToString();
        Debug.Log("Score Increased");
    }


    // Singleton 	
    private static ScoreManager instance;

    // Construct 	
    private ScoreManager() { }

    //  Instance 	
    public static ScoreManager Instance
    {
        get
        {
            if (instance == null)
                instance = GameObject.FindObjectOfType(typeof(ScoreManager)) as ScoreManager;
            return instance;
        }
    }

    public void YouLose()
    {
        // Hide Normal Score
        //ScoreText.gameObject.SetActive(false);
        // Show Game Over Panel
        GameOverPanel.SetActive(true);

        // Show score on Game Over Panel
        ScoreDead.text = "Score: " + ScoreInt.ToString();
        // Check if exists a high score
        if (PlayerPrefs.HasKey("HighScore")){
            // Store highscore locally
            int highscore = PlayerPrefs.GetInt("HighScore");

            // Compare Score and High Score
            if(ScoreInt > highscore)
            {
                // Save New High Score
                PlayerPrefs.SetInt("HighScore", ScoreInt);
                // Show High Score to player
                HighScore.text = "Best: " + ScoreInt.ToString();
            }
            else
            {
                // Show High Score
                HighScore.text = "Best: " + highscore.ToString();
            }
        }
        // If high score doesn't exists
        else
        {
            // Save New High Score
            PlayerPrefs.SetInt("HighScore", ScoreInt);
            // Show High Score to player
            HighScore.text = "Best: " + ScoreInt.ToString();
        }
    }
}
