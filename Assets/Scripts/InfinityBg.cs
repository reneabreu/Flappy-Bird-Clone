﻿using UnityEngine;
using System.Collections;

public class InfinityBg : MonoBehaviour {

    public enum Direction
    {
        Left,
        Right,
        Up,
        Down
    }

    public Direction direction;
	public float speed = 0.5f;

    private Vector3 startPosition;

    void Start()
    {
        Time.timeScale = 1;
    }

    Vector2 offset;

    void Update ()
    {
        switch (direction)
        {
            case Direction.Left:
                offset = new Vector2(Time.time * speed, 0);
                break;
            case Direction.Right:
                offset = new Vector2(-(Time.time * speed), 0);
                break;
            case Direction.Up:
                offset = new Vector2(0, -(Time.time * speed));
                break;
            case Direction.Down:
                offset = new Vector2(0, Time.time * speed);
                break;

        }
        GetComponent<Renderer>().material.mainTextureOffset = offset;

    }

}
