﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    [Tooltip("Jumping Aceleration")]
    public Vector2 aceleration = new Vector2(0, 300);           // Jumping Aceleration

	void Start(){
        // Prevent game from being paused
        Time.timeScale = 1;
	}

	void Update () {
        // Get Space Down Or Mouse Left Click (or touch in case of mobile)
		if (Input.GetKeyUp ("space") || Input.GetMouseButtonDown(0)) 
		{
            // Apply jumping aceleration
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            GetComponent<Rigidbody2D>().AddForce(aceleration);
        }

        // Die by being off screen
        Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
		if (screenPosition.y > Screen.height || screenPosition.y < 0)
		{
            // Dead :(
			Die();
		}
	}

    // Check Collision
	void OnCollisionEnter2D(Collision2D colisao)
	{
		if (colisao.gameObject.tag == "Obstacle")
			Die();
	}

	void Die()
	{
        // Stop everything from happening
        Time.timeScale = 0;

        // Call Game Over Event
        ScoreManager.Instance.YouLose();
	}
}
