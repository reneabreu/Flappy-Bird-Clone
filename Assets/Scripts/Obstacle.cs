﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour {

    [Tooltip("Velocity X and Y to apply in the obstacle")]
    public Vector2 velocity = new Vector2(-4, 0);                   // Velocity X and Y to apply in the obstacle
    [Tooltip("Range betweem obstacles Y position")]
    public float range = 4;                                         // Range betweem obstacles Y position

    // Use this for initialization
    void Start()
	{
        // Apply velocity
		GetComponent<Rigidbody2D>().velocity = velocity;
        // Apply new object Y position between original Y position and range
		transform.position = new Vector3 (transform.position.x, transform.position.y - range * Random.value, transform.position.z);
	}
	
    // Check Collision
	void OnTriggerEnter2D(Collider2D colisao)
	{
		//if (colisao.gameObject.tag == "destroy")
			//Destroy(this.gameObject);
	}
}